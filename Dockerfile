ARG VERSION=

FROM bitnami/fluentd:$VERSION

RUN fluent-gem install 'fluent-plugin-gelf-hs' && \
    fluent-gem install 'fluent-plugin-parser-cri'
