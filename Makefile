export VERSION=1.14.1-debian-10-r0
export NAME=fluentd
export IMG=registry.gitlab.com/jhmdocker/image/$(NAME):$(VERSION)

build:
	docker build --pull -t $(IMG) --build-arg=VERSION=$(VERSION) .
	docker push $(IMG)
